Rails.application.routes.draw do

  root 'home#index'
  get 'get/*query', to: 'products#index'  
  get 'terms-and-conditions', to:'home#terms'
  get 'privacy-policy', to:'home#privacy'
  post 'user_login', to: 'home#user_login'
  get 'login', to: 'home#login'
  get 'team', to: 'home#team'
  get 'careers', to: 'home#careers'

  get 'search/', to: 'products#index'
  get 'products/', to: 'products#index'
  get 'products/:category/:sub_category/:brand/:name', to: 'products#show'
  post 'products', to:'products#index'
  post 'suggested_list', to: 'products#suggested_list'
  

  get 'stores', to:'stores#index'
  get 'favourites-list', to: 'stores#favourites_list'
  get 'stores/:store_area/:name', to: 'stores#show'

  post 'stores', to:'stores#index'
  post 'set_favourite', to: 'stores#set_favourite'
  post 'remove_favourite', to: 'stores#remove_favourite'
  post 'favourites-list', to: 'stores#favourites_list'
  get 'profile', to: 'stores#profile'
  post 'profile', to: 'stores#profile'
  get 'my-profile', to: 'stores#my_profile'
  post 'my-profile', to: 'stores#my_profile'
  post 'set_name', to: 'stores#set_name_profile'
  post 'set_mobile', to: 'stores#set_mobile_profile'
  post 'set_profile', to: 'stores#set_profile'
  post 'feedback', to: 'stores#feedback'

  get '/blogs', to: 'blogs#index'
  get '/blogs/:title', to: 'blogs#show'
  get 'blogs/:title/change', to: 'blogs#edit'
  post 'blogs/update', to: 'blogs#update'
  post '/blogs/post_to_blog', to: 'blogs#post_to_blog'
  get '/write/a/blog/', to: 'blogs#new'
  post 'contact_us', to: 'user#contact_us'
  post 'send_resume', to: 'home#send_resume'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
