# config valid only for current version of Capistrano
#require 'whenever/capistrano'
lock '3.10.0'

set :application, 'website-mappr'
set :repo_url, 'git@gitlab.com:ashokkumar_mapprr/mapprr-web.git'
set :deploy_to, "/home/ec2-user/webapp/#{fetch(:application)}"
set :branch, "master"
set :rails_env, "production"

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')

set :pty, true
set :sidekiq_monit_conf_dir, '/etc/monit.d'

set :passenger_restart_with_touch, true
# set :passenger_in_gemfile, true
after 'deploy:publishing', 'deploy:restart'
set :bundle_binstubs, nil

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')
# set :linked_files, fetch(:linked_files, []).push('config/secrets.yml')



# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# namespace :deploy do

#   after :restart, :clear_cache do
#     on roles(:web), in: :groups, limit: 3, wait: 10 do
#       # Here we can do anything such as:
#       # within release_path do
#       #   execute :rake, 'cache:clear'
#       # end
#     end
#   end
# end