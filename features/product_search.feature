@headmode
Feature: This is where the product searching features will be tested

  Scenario: Product Search should have a auto suggest
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I search for product named "Crocin"
    Then I should see the product "Crocin"
    Then I logout

  Scenario: Should show a store in the location if the product is available
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I search for product named "Crocin"
    Then I click on the product "Crocin"
    Then I should see "Stores" on screen
    Then I logout

  Scenario: Should show a store in the location if the product is available
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I search for product named "AMOXYCILLIN"
    Then I click on the product "AMOXYCILLIN"
    Then I should see "Product Not Available At Your Nearest Store" on screen