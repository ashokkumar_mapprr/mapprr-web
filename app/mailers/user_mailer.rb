class UserMailer < ApplicationMailer
	default from: 'developer@mapprr.com'
 
  def send_contact_query(params)
    @name = params['name']
    @email  = params['email']
    @message = params['message']
    mail(to: 'ravi@mapprr.com',
    	subject: 'Contact Query',
    	template_name: 'contact_query')
  end

  def thank_you_mail(params)
  	@name = params['name']
  	@email = params['email']
  	mail(to: @email,
  		subject: 'Thank You - Mapprr',
  		template_name: 'thank_you')
  end

  def careers_mail(params)
    @name = params['name']
    @phone = params['phone']
    @email = params['email']
    @designation = params['designation']
    @description = params['description']
    attachments[params[:file].original_filename] = File.read(params[:file].tempfile.path)
    mail(to: 'careers@mapprr.com',
      subject: 'Resume - ' + @name,
      template_name: 'careers_mail')
  end
end