// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require rails-ujs
//= require ckeditor/init
//= require_tree

// $(window).load(function(){
//      $('.loader').fadeOut('slow');
// });
$("form#p_form").submit(function(e){
        gtag_report_conversion();
        window.localStorage.removeItem("search_query");
        var search = $("#search_query").val();
        window.localStorage.setItem("search_query", search);
        e.preventDefault();
        window.location.href = "/products?product="+search;
      });
//Location selection
var loc;
function detectLocation()
{
    // start detect location.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {

                  var latitude = position.coords.latitude;
                  var longitude = position.coords.longitude;
                  //console.log(latitude);
                  var geocoder;
                  geocoder = new google.maps.Geocoder();
                  var latlng = new google.maps.LatLng(latitude, longitude);
                  var latlong = latitude.toFixed(6) + "," + longitude.toFixed(6);
                  window.sessionStorage.setItem("latlng", latlong); // store lat, long values
                  geocoder.geocode({
                      'latLng': latlng
                  }, function(results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {

                          if (results[0]) {
                              var address = results[0].address_components;
                              var pincode = address[address.length - 1].long_name;
                              window.sessionStorage.setItem("pincode", pincode);
                              var add = results[0].formatted_address;
                              var value = add.split(',').slice(0,2).join(', ');
                              var element = document.getElementById('geolocation');
                              element.innerHTML = value;

                              window.sessionStorage.setItem("slocation", value); // save addrs
                               window.localStorage.setItem("slocation", value); // save addrs duplicate

                          } else {
                              alert("address not found");
                          }
                      } else {
                          //document.getElementById("location").innerHTML="Geocoder failed due to: " + status;
                          // alert("Geocoder failed due to: " + status);
                      }
                  });

          }, function() {
            handleLocationError(true, loc);
          });
        } else {
          // Browser doesn't support Geolocation
          handleLocationError(false, loc);
        }
      }

      function handleLocationError(browserHasGeolocation, loc) {
        // alert(browserHasGeolocation ?
        //                       'Error: The Geolocation service failed.' :
        //                       'Error: Your browser doesn\'t support geolocation.');

                var x = $("#location_error")
                      x.addClass("show")
                        .html(browserHasGeolocation ?
                              'Your location could not be determined.' :
                              'Error: Your browser doesn\'t support geolocation.');
                      setTimeout(function() {
                         x.removeClass("show");
                     }, 7000);
      }
// end detect location
//location selection manual
function initAutocomplete() {
        var input = document.getElementById('m_loc');

        var options = {
                            componentRestrictions: {country: 'in'}
                        };
        var autocomplete = new google.maps.places.Autocomplete(
            input, options);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('geolocation');
        infowindow.setContent(infowindowContent);

        var geocoder = new google.maps.Geocoder;
        autocomplete.addListener('place_changed', function() {

          var place = autocomplete.getPlace();

          if (!place.place_id) {
            return;
          }
          geocoder.geocode({'placeId': place.place_id}, function(results, status) {

            if (status !== 'OK') {
              //window.alert('Geocoder failed due to: ' + status);
              return;
            }
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            var latlong = latitude.toFixed(6) + "," + longitude.toFixed(6);
            window.sessionStorage.setItem("latlng", latlong); // store lat, long values
        //get pincode using latlng
            var geocoder;
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(latitude, longitude);
            geocoder.geocode({
                'latLng': latlng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    //$(".loading").hide();

                    if (results[0]) {
                        var address = results[0].address_components;
                        var pincode = address[address.length - 1].long_name;
                        window.sessionStorage.setItem("pincode", pincode);
                        //debugger

                    } else {
                        alert("address not found");
                    }
                } else {
                    //document.getElementById("location").innerHTML="Geocoder failed due to: " + status;
                    //alert("Geocoder failed due to: " + status);
                }
            });

            var add = results[0].formatted_address;
            var value = add.split(',').slice(0,2).join(', ');
            infowindowContent.textContent = value;
            window.sessionStorage.setItem("slocation", value);
            window.localStorage.setItem("slocation", value); //duplicate
             $('.c_loc').hide();
             $(".home-location").click(function(){
                        $('.c_loc').show();
             });

          });
        });
      }

