class ProductsController < ApplicationController
  def index
    if params[:product].present? || params[:query].present?
      params[:query].gsub!("$", " ")
      key_params = encode_key()
      products_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/products/search?key="+key_params), {})
      products_data = JSON.parse(products_data.body)
      @categories = products_data['categories']
      # binding.pry
      @suggested_query = products_data['suggested_query']
      if params['categories'].present?
        @products = []
        category_ids = params['categories'].split(",")
        products_data['products'].each do |p|
          if category_ids.include? p['sub_category_id'].to_s
            @products << p
          end
        end
      else
        @products = products_data['products']
      end
    else
      @products = "404"
    end
  end

  def show
    product_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/products/get_product?category="+params[:category].gsub("&", "$")+"&sub_category="+params[:sub_category]+"&brand="+params[:brand]+"&name="+params[:name]), {})
    @product_data = JSON.parse(product_data.body)
    if @product_data["product"].present?
      @product = @product_data["product"]
      @similar_products = @product_data["similar_products"]
      @image_sample = ['map1.png','map2.png','map3.png'].sample
    else
      @product = nil
    end
  end

  def suggested_list
    key_params = encode_key()
    suggested_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/products/search_suggestions?key="+key_params), {})
    suggested_data = JSON.parse(suggested_data.body)
    # binding.pry
    if not suggested_data.nil?
      render status: 200, json:{"suggestions" => suggested_data["suggestions"].collect{|x,y| x}}
    else
      render status: 401, json: {"message" => "No data sent"}
    end
  end

  private

  	def encode_key
  		query = params["product"] || params["query"]
  		iv = '1234567890123456'
  		api_key_hash = {"mapprr"=>"12345678908905672341765890432165"}
  		key = api_key_hash["mapprr"]
  		acipher = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
      acipher.encrypt
      acipher.key = key
      acipher.iv = iv
     	encrypted_string = acipher.update(query) + acipher.final
     	val = Base64.encode64(encrypted_string).strip
     	val
  	end

end
